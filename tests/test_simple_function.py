# python -m pytest - run this in the top level directory

from cods_testing.simple_function import add, multiply

import pytest

def test_add():
    assert add(1, 2) == 3
    assert add(-10, 10) == 0

def test_multiply():
    assert multiply(10000, 0) == 0
    assert multiply(-1, 10) == -10


