from cods_testing.simple_parametrize import add, multiply

import pytest

param_input_1 = (1, 2, 3)
param_input_2 = (1, 2, 2)

@pytest.mark.parametrize('x, y, result', [param_input_1])
def test_param_add(x, y, result):
    assert add(x, y) == result

@pytest.mark.parametrize('x, y, result', [param_input_2])
def test_param_multiply(x, y, result):
    assert multiply(x, y) == result


